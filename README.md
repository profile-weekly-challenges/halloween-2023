
# Reto Especial Halloween Profile 2023 - Truco o Trato

## Reglas del Reto
- El reto se puede realizar en solitario o con más compañeros, aunque el premio no se podrá adaptar a equipos demasiado grandes.
- Se debe de haber subido un FORK de este proyecto antes del día 29 de OCTUBRE.
- El FORK del proyecto debe de estar visible para que este sea validado e incluido en las votaciones.

## Especificaciones del Reto

- Se puede realizar en cualquier lenguaje y siguiendo cualquier estrategia.
- Se puede enfocar tanto de manera BACK, como de manera FRONT, e incluso híbrida.
- Se puntuará en base al código y al resultado final, amén de la puntuación de los participantes y el jurado.

## Enunciado del Reto
 Deberemos crear un programa al que le indiquemos si queremos realizar "Truco o Trato" y un listado de personas con las siguientes propiedades:
 - Nombre de la persona
 - Edad
 - Altura en centímetros
 
Esta información se podrá leer a través de un fichero, de manera manual desde la consola o de la manera que se prefiera.

  Si las personas han pedido truco, el programa retornará una serie de sustos aleatorios, entre los que se encuentran:
#### 🎃 👻 💀 🕷 🕸 🦇
  
  ### Para saber la cantidad de sustos que se deben mostrar, se considerará lo siguiente:
  - 1 susto por cada 2 letras del nombre por persona.
  - 2 sustos por cada edad que sea un número par.
  - 3 sustos por cada 100 cm de altura entre todas las personas.
 
 Si las personas han pedido trato, el programa retornará una serie de DULCES aleatorios, entre los que se encuentran:
#### 🍰 🍬 🍡 🍭 🍪 🍫 🧁 🍩
 ### Para saber la cantidad de DULCES que se deben mostrar, se considerará lo siguiente:
  - 1 dulce por cada letra de nombre
  - 2 dulce por cada 3 años cumplidos hasta un máximo de 10 años por persona
  - 3 dulces por cada 50 cm de altura hasta un máximo de 150 cm por persona

## ¡¡ PREMIO !!

A la solución más votada el día de Halloween se le recompensará con una bolsa de dulces y el aplauso de todos los participantes.

 
