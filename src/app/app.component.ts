import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


interface Person {
  name: string;
  age: number;
  size: number;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {

  listPersons: Person[] = [];

  tricks = ['🎃', '👻', '💀', '🕷', '🕸', '🦇'];
  treats = ['🍰', '🍬', '🍡', '🍭', '🍪', '🍫', '🧁', '🍩'];

  form: FormGroup = new FormGroup({});

  result: string[] = [];

  constructor(
    private formBuilder: FormBuilder,
  ){ 
    this.form = this.setForm();
  }

  addPerson() {
    this.listPersons.push({
      name: this.form.get('name')?.value,
      age: Number(this.form.get('age')?.value),
      size: Number(this.form.get('size')?.value),
    });
    this.form.reset();
  }

  private setForm(): FormGroup {
    const formGroup = this.formBuilder.group({
      name: this.formBuilder.control(null, [Validators.required]),
      age: this.formBuilder.control(null, [Validators.required]),
      size: this.formBuilder.control(null, [Validators.required]),
    });

    return formGroup;
  }

  runTrick() {
    let qty = 0;
    let sizes = 0;

    if(this.listPersons.length > 0){

      this.listPersons.forEach(person => {
        //NAME
        qty += Math.trunc(person.name.length/2);

        //AGE
        if(person.age % 2 === 0){
          qty += 2;
        }
        //SIZE
        sizes += person.size;
      });

      qty += Math.trunc(sizes/100)*3;
    }

    this.result = this.getTrickOrTreat(qty, this.tricks);
  }

  runTreat() {
    let qty = 0;

    this.listPersons.forEach(person => {
      //NAME
      qty += person.name.length;

      //AGE
      const triple = Math.trunc(person.age/3);
      qty += (triple > 3 ? 3 : triple)*2;

      //SIZE
      qty += Math.trunc(person.size/50)*3;
    });

    this.result = this.getTrickOrTreat(qty, this.treats);
  }

  private getTrickOrTreat = (qty: number, list: string[]): string[] => {
    let result = [];

    for (let i = 0; i < qty; i++) {
      let index = this.getRandomInt(list.length - 1);
      result.push(list[index]);
    }

    return result;
  }

  private getRandomInt(max: number) {
    return Math.floor(Math.random() * max);
  }
}
